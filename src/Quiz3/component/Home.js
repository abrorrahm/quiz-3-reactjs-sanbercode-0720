import React, {Component} from 'react';
import axios from 'axios';

class Home extends Component {
    constructor(props){
        super(props)
        this.state = {
            daftarFilm : []
        }
    }

    componentDidMount(){
        axios.get(`http://backendexample.sanbercloud.com/api/movies`)
            .then(res => {
                res.data.map(el=>{
                    this.setState({
                        daftarFilm : [...this.state.daftarFilm,{
                            title : el.title,
                            description : el.description,
                            year : el.year,
                            duration : el.duration,
                            genre : el.genre,
                            rating : el.rating
                        }]
                    })
              } )
            })
    }

    render(){
        return (
            <>
            <section>
                <h1>Daftar Film Terbaik</h1>
                {this.state.daftarFilm.map((val, index)=>{
                    return(
                        <div key={index}>
                            <h2>{val.title}</h2>
                            <p><strong>Rating: {val.rating}</strong></p>
                            <p><strong>Duration: {val.duration}</strong></p>
                            <p><strong>Genre: {val.genre}</strong></p>
                            <p><strong>Year: {val.year}</strong></p>
                            <p><strong>Description: {val.description}</strong></p><br></br>
                        </div>
                    )
                })}
            </section>
            <footer>
                <h5>copyright &copy; 2020 by Sanbercode</h5>
            </footer>
            </>
        )
    }
}
export default Home