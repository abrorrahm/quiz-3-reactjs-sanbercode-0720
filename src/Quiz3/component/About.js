import React from 'react';

function About(){
    const nama = "Nama: "
    const email = "Email: "
    const sistem = "Sistem Operasi yang digunakan: "
    const gitlab = "Akun Gitlab: "
    const telegram = "Akun Telegram: "

    return (
        <section>
            <div style={{padding: "10px", border: "1px solid #ccc"}}>
                <h1 style={{textAlign: "center"}}>Data Peserta Sanbercode Bootcamp Reactjs</h1>
                <ol>
                    <li><strong style={{width: "100px"}}>{nama} </strong>Abror Rahmatullah</li>
                    <li><strong style={{width: "100px"}}>{email} </strong>rahmatullahabror@gmail.com</li>
                    <li><strong style={{width: "100px"}}>{sistem} </strong>Windows</li>
                    <li><strong style={{width: "100px"}}>{gitlab} </strong>https://gitlab.com/abrorrahm</li>
                    <li><strong style={{width: "100px"}}>{telegram} </strong>https://t.me/AbrorRahmatullah</li>
                </ol>        
            </div>
        </section>
        
    )
}

export default About