import React from "react";
import { Switch, Route, Link } from "react-router-dom";
import Home from "./component/Home";
import About from "./component/About.js";
import MovieListEditor from "../Quiz3/MovieListEditor"
import Login from "../Quiz3/Login"
import logo from './public/img/logo.png'

const Routes = () => {

    return(
        <>
        <header>
        <img id="logo" src={ logo } width="200px" />
        <nav>
          <ul>
            <li><a><Link to="/">Home</Link></a></li>
            <li><a><Link to="/about">About</Link></a></li>
            <li><a><Link to="/movielisteditor">Movie List Editor</Link></a></li><small>(Access with token only)</small>
            <li><a><Link to="/login">Login</Link></a></li><small>(Access without token only)</small>
          </ul>
        </nav>
      </header>
        <Switch>
            <Route exact path='/' component={Home}/>
            <Route path='/about' component={About}/>
            <Route path='/movielisteditor' component={MovieListEditor}/>
            <Route path='/login' component={Login}/>
        </Switch>
    </>
    )
};

export default Routes;