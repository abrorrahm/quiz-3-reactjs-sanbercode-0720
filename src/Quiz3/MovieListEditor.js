import React, {useEffect, useState} from "react"
import axios from "axios"

const MovieListEditor = () =>{
    const [daftarFilm, setDaftarFilm] = useState(null)
    const [inputTitle, setInputTitle] = useState("")
    const [inputDescription, setInputDescription] = useState("")
    const [inputYear, setInputYear] = useState(0)
    const [inputDuration, setInputDuration] = useState(0)
    const [inputGenre, setInputGenre] = useState("")
    const [inputRating, setInputRating] = useState(0)
    const [selectedId, setSelectedId] = useState(0)
    const [statusForm, setStatusForm] = useState("create")

    const handleLogout = () => {    
        this.history.push('/login');
    }

    useEffect( () => {
        if (daftarFilm === null){
          axios.get(`http://backendexample.sanbercloud.com/api/movies`)
          .then(res => {
            setDaftarFilm(res.data.map(el=>{ return {id: el.id, title: el.title, rating:el.rating, duration:el.duration, genre:el.genre, year:el.year, description: el.description}} ))
          })
        }
    }, [daftarFilm])
    
    const handleDelete = (event) => {
        let idFilm = parseInt(event.target.value)
        
        let newDaftarFilm = daftarFilm.filter(el => el.id !== idFilm)

        axios.delete(`http://backendexample.sanbercloud.com/api/movies/${idFilm}`)
        .then(res => {
        console.log(res)
        })
        setDaftarFilm([...newDaftarFilm])
    }

    const handleEdit = (event) =>{
        let idFilm = parseInt(event.target.value)
        let dataFilm = daftarFilm.find(x=> x.id === idFilm)
        setInputTitle(dataFilm.title)
        setInputDescription(dataFilm.description)
        setInputYear(dataFilm.year)
        setInputDuration(dataFilm.duration)
        setInputGenre(dataFilm.genre)
        setInputRating(dataFilm.rating)
        setSelectedId(idFilm)
        setStatusForm("edit")
      }

    const handleChangeTitle = (event) => {
        setInputTitle(event.target.value)
    }
    const handleChangeDescription = (event) => {
        setInputDescription(event.target.value)
    }
    const handleChangeYear = (event) => {
        setInputYear(event.target.value)
    }
    const handleChangeDuration = (event) => {
        setInputDuration(event.target.value)
    }
    const handleChangeGenre = (event) => {
        setInputGenre(event.target.value)
    }
    const handleChangeRating = (event) => {
        setInputRating(event.target.value)
    }

    const handleSubmit = (event) =>{
        event.preventDefault();

        let title = inputTitle
        let description = inputDescription
        let year = inputYear
        let duration = inputDuration
        let genre = inputGenre
        let rating = inputRating

        if (title.replace(/\s/g,'') !== "" && description.replace(/\s/g,'') !== "" && year.toString().replace(/\s/g,'') !== ""  && duration.toString().replace(/\s/g,'') !== "" && genre.replace(/\s/g,'') !== "" && rating.toString().replace(/\s/g,'') !== ""){      
            if (statusForm === "create"){        
                axios.post(`http://backendexample.sanbercloud.com/api/movies`, {title, description, year, duration, genre, rating})
                .then(res => {
                    setDaftarFilm([...daftarFilm, {id: res.data.id, title: title, rating: rating, duration: duration, genre: genre, year: year, description: description}])
                })
            }else if(statusForm === "edit"){
                axios.put(`http://backendexample.sanbercloud.com/api/movies/${selectedId}`, {title, description, year, duration, genre, rating})
                .then(res => {
                    let dataFilm = daftarFilm.find(el=> el.id === selectedId)
                    dataFilm.title = title
                    dataFilm.description = description
                    dataFilm.year = year
                    dataFilm.duration = duration
                    dataFilm.genre = genre
                    dataFilm.rating = rating
                    setDaftarFilm([...daftarFilm])
                })
            }
            
            setStatusForm("create")
            setSelectedId(0)
            setInputTitle("")
            setInputDescription("")
            setInputYear(0)
            setInputDuration(0)
            setInputGenre("")
            setInputRating(0)
        }
    }
  

  return(
    <>
    <section>
    <div className='table'>
        <h1>Movie List Editor</h1>
        <table>
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Year</th>
                    <th>Duration</th>
                    <th>Genre</th>
                    <th>Rating</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                {
                  daftarFilm !== null && daftarFilm.map((val, index)=>{
                    return(                    
                      <tr key={index}>
                        <td>{val.title}</td>
                        <td>{val.description}</td>
                        <td>{val.year}</td>
                        <td>{val.duration}</td>
                        <td>{val.genre}</td>
                        <td>{val.rating}</td>
                        <td>
                          <button onClick={handleEdit} value={val.id}>Edit</button>
                          &nbsp;
                          <button onClick={handleDelete} value={val.id}>Delete</button>
                        </td>
                      </tr>
                    )
                  })
                }
            </tbody>
        </table>
        </div>
        {/* Form */}
        <h1>Movie List Editor Form</h1>

        <div style={{width: "50%", margin: "0 auto", display: "block"}}>
            <div style={{border: "1px solid #aaa", padding: "20px"}}>
            <form onSubmit={handleSubmit}>
            <label style={{float: "left"}}>
            {/* title, description, year, duration, genre, rating */}
                    Title:
                    </label>
                    <input style={{float: "right"}} type="text" name="title" value={inputTitle} onChange={handleChangeTitle}/>
                    <br/>
                    <br/>
                    <label style={{float: "left"}}>
                    Description:
                    </label>
                    <textarea style={{float: "right"}} name="description" value={inputDescription} onChange={handleChangeDescription}/>
                    <br/>
                    <br/>
                    <br/>
                    <label style={{float: "left"}}>
                    Year:
                    </label>
                    <input style={{float: "right"}} type="number" name="year" value={inputYear} onChange={handleChangeYear}/>
                    <br/>
                    <br/>
                    <label style={{float: "left"}}>
                    Duration:
                    </label>
                    <input style={{float: "right"}} type="number" name="duration" value={inputDuration} onChange={handleChangeDuration}/>
                    <br/>
                    <br/>
                    <label style={{float: "left"}}>
                    Genre:
                    </label>
                    <input style={{float: "right"}} type="text" name="genre" value={inputGenre} onChange={handleChangeGenre}/>
                    <br/>
                    <br/>
                    <label style={{float: "left"}}>
                    Rating:
                    </label>
                    <input style={{float: "right"}} type="number" name="rating" value={inputRating} onChange={handleChangeRating}/>
                    <br/>
                    <br/>
                    <div style={{width: "100%", paddingBottom: "20px"}}>
                    <button style={{ float: "right"}}>submit</button>
                    </div>
            </form>
            </div>
            <input type="button" onClick={handleLogout} value="Logout" />
        </div>
    </section>
    
    </>
  )
}

export default MovieListEditor
